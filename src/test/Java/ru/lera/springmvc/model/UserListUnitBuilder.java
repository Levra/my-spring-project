package ru.lera.springmvc.model;

public class UserListUnitBuilder {

    private UserListUnit model;


    public UserListUnitBuilder user(User user){
        model=new UserListUnit(user);
        return this;
    }

    public UserListUnit build(){
        return model;
    }
}
