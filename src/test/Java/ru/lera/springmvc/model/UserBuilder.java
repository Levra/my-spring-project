package ru.lera.springmvc.model;

import java.util.Date;
import java.util.Set;

public class UserBuilder {
    private User model;

    public UserBuilder(){
        model=new User();
    }

    public UserBuilder id(Integer id){
        model.update(id,model.getUsername(),model.getNick(),model.getPassword(),model.getCreatedAt(),model.getUserProfiles());
        return this;
    }

    public UserBuilder username(String username) {
        model.update(model.getId(),username,model.getNick(),model.getPassword(),model.getCreatedAt(),model.getUserProfiles());
        return this;
    }

    public UserBuilder nick(String nick){
        model.update(model.getId(),model.getUsername(),nick,model.getPassword(),model.getCreatedAt(),model.getUserProfiles());
        return this;
    }


    public UserBuilder password(String password) {
        model.update(model.getId(),model.getUsername(),model.getNick(),password,model.getCreatedAt(),model.getUserProfiles());
        return this;
    }

    public UserBuilder createdAt(Date date){
        model.update(model.getId(),model.getUsername(),model.getNick(),model.getPassword(),date,model.getUserProfiles());
        return this;
    }

    public UserBuilder role(Set<UserProfile> userProfile){
        model.update(model.getId(),model.getUsername(),model.getNick(),model.getPassword(),model.getCreatedAt(),userProfile);
        return this;
    }

    public User build() {
        return model;
    }
}
