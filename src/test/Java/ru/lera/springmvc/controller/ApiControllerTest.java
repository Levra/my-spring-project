package ru.lera.springmvc.controller;

import org.springframework.security.web.FilterChainProxy;
import ru.lera.springmvc.Exeption.UserNotFoundExeption;
import ru.lera.springmvc.configuration.AppConfig;
import ru.lera.springmvc.configuration.TestContext;
import ru.lera.springmvc.configuration.TestUtil;
import ru.lera.springmvc.security.SecurityConfiguration;
import ru.lera.springmvc.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.lera.springmvc.model.User;
import ru.lera.springmvc.model.UserBuilder;
import ru.lera.springmvc.model.UserListUnit;
import ru.lera.springmvc.model.UserListUnitBuilder;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class,AppConfig.class,SecurityConfiguration.class})
@WebAppConfiguration
public class ApiControllerTest {

    private MockMvc mockMvc;

    @Resource
    private FilterChainProxy springSecurityFilterChain;

    @Autowired
    private UserService userService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        Mockito.reset(userService);

        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .alwaysDo(print())
                .apply(springSecurity())
                .addFilters(springSecurityFilterChain)
                .build();
    }

    @Test
    @WithMockUser(roles ="WRITER")
    public void add_emptyUser_ShouldReturnValidationError() throws Exception{
        User user=new User();

        mockMvc.perform(post("/api/add-user")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(user))
        )
                .andExpect(authenticated().withRoles("WRITER"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.fieldErrors", hasSize(3)))
                .andExpect(jsonPath("$.fieldErrors[2].path", is("username")))
                .andExpect(jsonPath("$.fieldErrors[2].message", is("Username can not be blank.")))
                .andExpect(jsonPath("$.fieldErrors[1].path", is("password")))
                .andExpect(jsonPath("$.fieldErrors[1].message", is("Password can not be blank.")))
                .andExpect(jsonPath("$.fieldErrors[0].path", is("nick")))
                .andExpect(jsonPath("$.fieldErrors[0].message", is("Nick name can not be blank.")));

        verifyZeroInteractions(userService);
    }

    @Test
    @WithMockUser(roles="WRITER")
    public void addUser_shouldAddNewUserAndReturnAddedUser() throws Exception{
        User user=new UserBuilder().nick("Levra").username("lera").password("1234").build();

        mockMvc.perform(post("/api/add-user")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(user))
        )
                .andExpect(authenticated().withRoles("WRITER"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.password", is("1234")))
                .andExpect(jsonPath("$.nick", is("Levra")))
                .andExpect(jsonPath("$.username", is("lera")));

        verify(userService).saveUser(any(User.class));
    }

    @Test
    @WithMockUser(roles = "WRITER")
    public void deleteById_UserIsNotFound_ShouldReturnHttpStatusCode404() throws Exception {
        when(userService.findById(3)).thenThrow(new UserNotFoundExeption(""));

        mockMvc.perform(delete("/api/delete-user/{id}", 3))
                .andExpect(authenticated().withRoles("WRITER"))
                .andExpect(status().isNotFound());

    }

    @Test
    @WithMockUser(roles={"WRITER"})
    public void deleteUserById_UserFound_ShouldDeleteUserAndReturnIt() throws Exception{
        int id=1;

        User deletedUser=new UserBuilder().id(id).username("lera").nick("Levra").password("1234").build();

        when(userService.findById(id)).thenReturn(deletedUser);

        mockMvc.perform(delete("/api/delete-user/{id}", id)
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(authenticated().withRoles("WRITER"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.username", is("lera")))
                .andExpect(jsonPath("$.nick", is("Levra")));

        verify(userService).deleteUserById(any(Integer.TYPE));
    }

    @Test
    @WithMockUser(roles = "WRITER")
    public void update_UserFound_ShouldUpdateUserAndReturnIt() throws Exception{
        int id=1;
        User user=new UserBuilder().id(id).username("oleg").nick("Olej").password("5678").build();
        User updatedUser=new UserBuilder().id(id).username("lera").nick("Levra").password("1234").build();

        when(userService.findById(any(Integer.TYPE))).thenReturn(user);
        when(userService.updateUser(any(User.class))).thenReturn(updatedUser);

        mockMvc.perform(patch("/api/update-user/{id}",id)
                .content(TestUtil.convertObjectToJsonBytes(updatedUser))
                .contentType(TestUtil.APPLICATION_JSON_UTF8))

                .andExpect(authenticated().withRoles("WRITER"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(id)))
                .andExpect(jsonPath("$.username", is("lera")))
                .andExpect(jsonPath("$.nick", is("Levra")))
                .andExpect(jsonPath("$.password", is("1234")));

        verify(userService).updateUser(any(User.class));
    }

    @Test
    @WithMockUser(roles = "WRITER")
    public void update_EmptyUser_ShouldReturnValidationError() throws Exception{
        int id=1;
        User user=new UserBuilder().id(id).build();

        mockMvc.perform(patch("/api/update-user/{id}", id)
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(user))
        )
                .andExpect(authenticated().withRoles("WRITER"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.fieldErrors", hasSize(3)))
                .andExpect(jsonPath("$.fieldErrors[2].path", is("username")))
                .andExpect(jsonPath("$.fieldErrors[2].message", is("Username can not be blank.")))
                .andExpect(jsonPath("$.fieldErrors[1].path", is("password")))
                .andExpect(jsonPath("$.fieldErrors[1].message", is("Password can not be blank.")))
                .andExpect(jsonPath("$.fieldErrors[0].path", is("nick")))
                .andExpect(jsonPath("$.fieldErrors[0].message", is("Nick name can not be blank.")));

        verifyZeroInteractions(userService);
    }

    @Test
    @WithMockUser(roles = "WRITER")
    public void update_UserNotFound_ShouldReturnHttpStatusCode404() throws Exception {
        int id=5;
        User updatedUser=new UserBuilder().username("oleg").nick("Olej").password("5678").build();
        User user=new UserBuilder().id(id).build();

        when(userService.findById(any(Integer.TYPE))).thenReturn(user);
        when(userService.updateUser(any(User.class))).thenThrow(new UserNotFoundExeption(""));

        mockMvc.perform(patch("/api/update-user/{id}", id)
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedUser))
        )
                .andExpect(authenticated().withRoles("WRITER"))
                .andExpect(status().isNotFound());

        verify(userService).findById(any(Integer.TYPE));
        verify(userService).updateUser(any(User.class));
    }

    @Test
    @WithMockUser(roles={"READER"})
    public void findAll_UsersFound_ShouldReturnFoundUsers() throws Exception{
        User firstUser=new UserBuilder().id(1).username("lera").nick("Levra").password("1234").createdAt(new Date()).build();
        UserListUnit first=new UserListUnitBuilder().user(firstUser).build();
        User secondUser=new UserBuilder().id(2).username("oleg").nick("Olej").password("5678").createdAt(new Date()).build();
        UserListUnit second=new UserListUnitBuilder().user(secondUser).build();

        int page=1,size=5;

        when(userService.findAllUsers(1,5)).thenReturn(Arrays.asList(first, second));

        mockMvc
                .perform(get("/api/users").accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(authenticated().withRoles("READER"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].nick", is("Levra")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].nick", is("Olej")));

        verify(userService, times(1)).findAllUsers(page,size);
        verifyNoMoreInteractions(userService);
    }

    @Test
    @WithMockUser(roles={"READER"})
    public void findById() throws Exception{
        int id=1;
        User user=new UserBuilder().id(id).username("lera").nick("Levra").password("1234").build();

        when(userService.findById(id)).thenReturn(user);

        mockMvc
                .perform(get("/api/user/{id}",id)
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(authenticated().withRoles("READER"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.id", is(id)))
                .andExpect(jsonPath("$.username", is("lera")))
                .andExpect(jsonPath("$.nick", is("Levra")));

        verify(userService, times(1)).findById(id);
        verifyNoMoreInteractions(userService);
    }

    @Test
    @WithMockUser(roles={"READER"})
    public void findById_UserNotFound_ShouldReturnHttpStatusCode404() throws Exception {

        when(userService.findById(3)).thenThrow(new UserNotFoundExeption(""));

        mockMvc.perform(get("/api/user/{id}", 3))
                .andExpect(status().isNotFound())
        .andExpect(authenticated().withRoles("READER"));

        verify(userService, times(1)).findById(3);
        verifyNoMoreInteractions(userService);
    }
}
