package ru.lera.springmvc.Exeption;

public class UserNotFoundExeption extends Exception {

    public UserNotFoundExeption(String message) {
        super(message);
    }
}
