package ru.lera.springmvc.model;

import ru.lera.springmvc.utils.DateUtils;

public class UserListUnit {

    public UserListUnit(User user){
        this.id=user.getId();
        this.nick=user.getNick();
        this.createdAt= DateUtils.convertToISOString(user.getCreatedAt());
    }

    private int id;

    private String nick;

    private String createdAt;

    private UserListUnit createdBy;

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public UserListUnit getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserListUnit createdBy) {
        this.createdBy = createdBy;
    }

    public void update(Integer id,String nick) {
        this.id=id;
        this.nick = nick;
    }
}
