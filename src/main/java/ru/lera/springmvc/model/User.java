package ru.lera.springmvc.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="app_user")
public class User implements Serializable{


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@NotEmpty
	@Column(name="username", unique=true, nullable=false)
	private String username;
	
	@NotEmpty
	@Column(name="password", nullable=false)
	private String password;

	@NotEmpty
	@Column(name="nick",unique = true, nullable=false)
	private String nick;

	public Date getCreatedAt(){return createdAt;}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@JsonIgnore
	@CreationTimestamp
	@Column(name="created_at")
	private Date createdAt;

	public Integer getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(Integer creatorId) {
		this.creatorId = creatorId;
	}

	@JsonIgnore
	@Column(name="creator_id",columnDefinition = "bigint default 0")
	private Integer creatorId;

	@Transient
	private User createdBy;

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	@Transient
	private String createdAtSring;

	public String getCreatedAtSring() {
		return createdAtSring;
	}

	public void setCreatedAtSring(String createdAtSring) {
		this.createdAtSring = createdAtSring;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "app_user_user_profile",
             joinColumns = { @JoinColumn(name = "user_id") },
             inverseJoinColumns = { @JoinColumn(name = "user_profile_id") })
	@JsonIgnore
	private Set<UserProfile> userProfiles = new HashSet<UserProfile>();

	public void update(Integer id,String username,String nick, String password,Date date, Set<UserProfile> userProfiles) {
		this.id=id;
		this.username = username;
		this.nick = nick;
		this.password=password;
		this.createdAt=date;
		this.userProfiles=userProfiles;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public Set<UserProfile> getUserProfiles() {
		return userProfiles;
	}

	public void setUserProfiles(Set<UserProfile> userProfiles) {
		this.userProfiles = userProfiles;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof User))
			return false;
		User other = (User) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	/*
	 * DO-NOT-INCLUDE passwords in toString function.
	 * It is done here just for convenience purpose.
	 */
	@Override
	public String toString() {
		return "User [id=" + id +  ", nick=" + nick + ", username=" + username + ", password=" + password + "]";
	}

	
}
