package ru.lera.springmvc.model;

import java.io.Serializable;

public enum UserProfileType implements Serializable{
	READER("READER"),
	WRITER("WRITER");
	
	String userProfileType;
	
	private UserProfileType(String userProfileType){
		this.userProfileType = userProfileType;
	}
	
	public String getUserProfileType(){
		return userProfileType;
	}
	
}
