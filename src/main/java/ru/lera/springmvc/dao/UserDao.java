package ru.lera.springmvc.dao;

import java.util.List;

import ru.lera.springmvc.Exeption.UserNotFoundExeption;
import ru.lera.springmvc.model.User;
import ru.lera.springmvc.model.UserListUnit;


public interface UserDao {

	User findById(int id) throws UserNotFoundExeption;
	
	User findByUsername(String username);
	
	void save(User user);

	void deleteById(int id);

	void deleteByUsername(String username);
	
	List<UserListUnit> findAllUsers(Integer page, Integer size);

}

