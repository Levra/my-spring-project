package ru.lera.springmvc.dao;

import java.util.ArrayList;
import java.util.List;

import ru.lera.springmvc.model.UserListUnit;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import ru.lera.springmvc.model.User;
import ru.lera.springmvc.utils.DateUtils;


@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

	static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);
	
	public User findById(int id) {
		User user = getByKey(id);
		if(user!=null){
			Hibernate.initialize(user.getUserProfiles());
		}
		Integer creatorId=user.getCreatorId();
		if(creatorId!=null) {
			user.setCreatedBy(this.findById(creatorId));
		}
		user.setCreatedAtSring(DateUtils.convertToISOString(user.getCreatedAt()));
		return user;
	}

	public User findByUsername(String username) {
		logger.info("Username : {}", username);
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("username", username));
		User user = (User)crit.uniqueResult();
		if(user!=null){
			Hibernate.initialize(user.getUserProfiles());
		}
		return user;
	}

	@SuppressWarnings("unchecked")
	public List<UserListUnit> findAllUsers(Integer page, Integer size) {

		int totalResult =((Long)getSession()
				.createCriteria(User.class)
				.setProjection(Projections.rowCount())
				.list()
				.get(0)).intValue();

		int totalPages = (int) Math.ceil(totalResult / size);

		int firstResult = 0;
		if (page >= 0 && page < totalPages) {
			firstResult = page * size;
		}
		int count = Math.min(size, totalResult - firstResult);

		List<User> users = (List<User>)getSession()
				.createCriteria(User.class)
				.setFirstResult(firstResult)
				.setMaxResults(count)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.addOrder(Order.asc("nick"))
				.list();

		List<UserListUnit> userListUnits=new ArrayList<>();
		users.forEach(user -> {
			UserListUnit unit = new UserListUnit(user);
			if(user.getCreatorId()!=null)
				unit.setCreatedBy(new UserListUnit(this.findById(user.getCreatorId())));
			userListUnits.add(unit);
		});


		return userListUnits;
	}

	public void save(User user) {
		persist(user);
	}

	@Override
	public void deleteById(int id) {
		delete(findById(id));
	}

	public void deleteByUsername(String username) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("username", username));
		User user = (User)crit.uniqueResult();
		delete(user);
	}

}
