package ru.lera.springmvc.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ValidationError {
    private List<FieldError> fieldErrors = new ArrayList<FieldError>();

    public ValidationError() {

    }

    public void addFieldError(String path, String message) {
        FieldError fieldError = new FieldError(path, message);
        fieldErrors.add(fieldError);
    }

    public List<FieldError> getFieldErrors() {
        Collections.sort(fieldErrors, new FieldErrorComparator());
        return fieldErrors;
    }
}
