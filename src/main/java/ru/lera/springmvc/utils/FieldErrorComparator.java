package ru.lera.springmvc.utils;

import java.util.Comparator;

/**
 * Created by Валерия Шахмартова on 08.09.2017.
 */
public class FieldErrorComparator implements Comparator<FieldError> {

    public int compare(FieldError e1, FieldError e2) {
        return e1.getPath().compareTo(e2.getPath());
    }

}
