package ru.lera.springmvc.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
    public static String convertToISOString(Date jsonDate){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        String nowAsISO = df.format(jsonDate);
        return nowAsISO;
    }
}
