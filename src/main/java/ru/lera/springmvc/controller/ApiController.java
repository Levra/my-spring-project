package ru.lera.springmvc.controller;

import ru.lera.springmvc.Exeption.UserNotFoundExeption;
import ru.lera.springmvc.model.User;
import ru.lera.springmvc.model.UserListUnit;
import ru.lera.springmvc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


import javax.validation.Valid;
import java.util.List;


@RestController
@EnableWebMvc
@RequestMapping("/api")
@SessionAttributes("roles")
public class ApiController {

    @Autowired
    UserService userService;

    private final UserService service;

    private final MessageSource messageSource;

    @Autowired
    public ApiController(MessageSource messageSource, UserService service) {
        this.messageSource = messageSource;
        this.service = service;
    }

    /**
     * POST
     * @param user Сущность пользователя
     * @return сущность пользователя
     */
    @RequestMapping(value = "/add-user", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public User addUser(@Valid @RequestBody User user) {
       userService.saveUser(user);
       return user;
    }

    /**
     * GET
     * Получает пользователя по id
     * @param id ID пользователя
     * @return сущность пользователя
     */
    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    @ResponseBody
    public User getUser(@PathVariable("id") Integer id) throws UserNotFoundExeption {
        User user=userService.findById(id);
        return user;
    }

    /**
     * GET
     * Получает список всех пользователей
     * @return Список всех пользователей
     */
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @ResponseBody
    public List<UserListUnit> getUsers(@RequestParam( value="page",required = false,defaultValue = "1") Integer page,
                                       @RequestParam( value="size",required = false, defaultValue = "5") Integer size) {

        return userService.findAllUsers(page,size);
    }

    /**
     * PATCH
     * Обновляет информацию о пользователе с данным ID
     * Метод доступен только для WRITER
     * @param id ID пользователя
     * @param updatedUser Сущеность пользователя
     * @return Сущность пользователя
     */
    @RequestMapping(value = "/update-user/{id}", method = RequestMethod.PATCH)
    @ResponseBody
    public User updateUser(
            @PathVariable("id") Integer id,@Valid @RequestBody User updatedUser) throws UserNotFoundExeption{
        User user =userService.findById(id);
        user.setUsername(updatedUser.getUsername());
        user.setPassword(updatedUser.getPassword());
        user.setNick(updatedUser.getNick());
        userService.updateUser(user);
        return user;
    }

    /**
     * DELETE
     * Удаляет пользователя с заданным ID
     * Метод доступен только для WRITER
     * @param id ID пользователя
     * @return Сущность пользователя\
     */
    @RequestMapping(value = "/delete-user/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public User deleteUser(@PathVariable("id") Integer id) throws UserNotFoundExeption{
        User user=userService.findById(id);
        userService.deleteUserById(id);
        return user;
    }

}
