package ru.lera.springmvc.service;

import java.util.List;

import ru.lera.springmvc.Exeption.UserNotFoundExeption;
import ru.lera.springmvc.model.UserListUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.lera.springmvc.dao.UserDao;
import ru.lera.springmvc.model.User;


@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDao dao;

	@Autowired
    private PasswordEncoder passwordEncoder;

	@Override
	public User findById(int id) throws UserNotFoundExeption {
		return dao.findById(id);
	}

	@Override
	public User findByUsername(String username) {
		User user = dao.findByUsername(username);
		return user;
	}

	@Override
	public void saveUser(User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		user.setCreatorId(this.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId());
		dao.save(user);
	}

	@Override
	public User updateUser(User user) throws UserNotFoundExeption{
		User entity = dao.findById(user.getId());
		if(entity!=null){
			entity.setUsername(user.getUsername());
			if(!user.getPassword().equals(entity.getPassword())){
				entity.setPassword(passwordEncoder.encode(user.getPassword()));
			}
			entity.setNick(user.getNick());
			entity.setUserProfiles(user.getUserProfiles());
		}
		return user;
	}

	@Override
	public void deleteUserById(int id) {
		dao.deleteById(id);
	}

	@Override
	public void deleteUserByUsername(String username) {
		dao.deleteByUsername(username);
	}

	@Override
	public List<UserListUnit> findAllUsers(Integer page, Integer size) {
		return dao.findAllUsers(page,size);
	}

}
