package ru.lera.springmvc.service;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import ru.lera.springmvc.Exeption.UserNotFoundExeption;
import ru.lera.springmvc.model.User;
import ru.lera.springmvc.model.UserListUnit;


public interface UserService {

	@PreAuthorize("hasAnyRole('WRITER','READER')")
	User findById(int id) throws UserNotFoundExeption;

	@PreAuthorize("hasAnyRole('WRITER','READER')")
	User findByUsername(String username);

	@PreAuthorize("hasRole('WRITER')")
	void saveUser(User user);

	@PreAuthorize("hasRole('WRITER')")
	User updateUser(User user) throws UserNotFoundExeption;

	@PreAuthorize("hasRole('WRITER')")
	void deleteUserById(int id);

	@PreAuthorize("hasRole('WRITER')")
	void deleteUserByUsername(String username);

	@PreAuthorize("hasAnyRole('WRITER','READER')")
	List<UserListUnit> findAllUsers(Integer page, Integer size);


}