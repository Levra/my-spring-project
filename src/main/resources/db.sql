CREATE TABLE app_user (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  username varchar(30) NOT NULL,
  password varchar(100) NOT NULL,
  nick varchar(30) NOT NULL,
  created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE KEY (username, nick)
) AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

create table USER_PROFILE(
  id BIGINT NOT NULL AUTO_INCREMENT,
  type VARCHAR(30) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (type)
);

CREATE TABLE APP_USER_USER_PROFILE (
  user_id BIGINT NOT NULL,
  user_profile_id BIGINT NOT NULL,
  PRIMARY KEY (user_id, user_profile_id),
  CONSTRAINT FK_APP_USER FOREIGN KEY (user_id) REFERENCES APP_USER (id),
  CONSTRAINT FK_USER_PROFILE FOREIGN KEY (user_profile_id) REFERENCES USER_PROFILE (id)
);

INSERT INTO USER_PROFILE(type)
VALUES ('READER');

INSERT INTO USER_PROFILE(type)
VALUES ('WRITER');


INSERT INTO APP_USER(username, password, nick)
VALUES ('lera','1234', 'Levra');

INSERT INTO APP_USER(username, password, nick)
VALUES ('oleg','5678', 'Olej');

INSERT INTO APP_USER_USER_PROFILE (user_id, user_profile_id)
  SELECT user.id, profile.id FROM app_user user, user_profile profile
  where user.username='lera' and profile.type='WRITER';

INSERT INTO APP_USER_USER_PROFILE (user_id, user_profile_id)
  SELECT user.id, profile.id FROM app_user user, user_profile profile
  where user.username='oleg' and profile.type='READER';

alter table app_user add  creator_id bigint(20);
alter table app_user add constraint NFK_APP_USER FOREIGN KEY (creator_id) REFERENCES APP_USER (id);
